var productosObtenidos;

function getProductos(){

var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
var request = new XMLHttpRequest();
/*configuracion de la peticion*/
request.onreadystatechange = function(){
  /*200 respondio bien, status 4= cargado, continuar*/
  if (this.readyState == 4 && this.status == 200) {
    /*pintar el resultado en la cosnole f12*/
      console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();

  }
}

request.open("GET",url,true);
request.send();

}

function procesarProductos(){
    var JSONProductos = JSON.parse(productosObtenidos);
    var divTabla = document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    //te pinta el primer renglon oscuro el siguiente en blanco y asi.
    tabla.classList.add("table-striped");

    //alert(JSONProductos.value[0].ProductName);
    for (var i = 0; i < JSONProductos.value.length; i++) {
    //  console.log(JSONProductos.value[i].ProductName);
        var nuevaFila = document.createElement("tr");
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONProductos.value[i].ProductName;
        var columnaPrecio = document.createElement("td");
        columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
        var columnaStock = document.createElement("td");
        columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaStock);

        tbody.appendChild(nuevaFila);
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}
